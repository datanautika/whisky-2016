'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Textarea = require('./Textarea.css');

var _Textarea2 = _interopRequireDefault(_Textarea);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

let Textarea = function (_React$Component) {
	_inherits(Textarea, _React$Component);

	function Textarea() {
		var _temp, _this, _ret;

		_classCallCheck(this, Textarea);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = _possibleConstructorReturn(this, (Textarea.__proto__ || Object.getPrototypeOf(Textarea)).call(this, ...args)), _this), _this.handleInput = event => {
			if (_this.props.handleChange) {
				_this.props.handleChange(_this.validate(event.target.value));
			}
		}, _this.handleFocusOut = event => {
			if (_this.props.handleSave) {
				_this.props.handleSave(_this.validate(event.target.value));
			}
		}, _temp), _possibleConstructorReturn(_this, _ret);
	}

	_createClass(Textarea, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(newProps) {
			return newProps.id !== this.props.id || newProps.name !== this.props.name || newProps.rows !== this.props.rows || newProps.isValid !== this.props.isValid || newProps.isInvalid !== this.props.isInvalid || newProps.isDisabled !== this.props.isDisabled || newProps.handleChange !== this.props.handleChange || newProps.handleSave !== this.props.handleSave || newProps.validator !== this.props.validator || newProps.value !== this.props.value;
		}
	}, {
		key: 'render',
		value: function render() {
			let textareaProps = {
				key: this.props.id || this.props.name || '',
				className: _Textarea2.default.default + (this.props.isInvalid ? ' isInvalid' : '') + (this.props.isValid ? ' isValid' : '') + (this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
				name: this.props.name || this.props.id || '',
				id: this.props.id || this.props.name || '',
				rows: this.props.rows || 5,
				onBlur: this.handleFocusOut,
				onChange: this.handleInput,
				value: this.props.value
			};

			if (this.props.isDisabled) {
				textareaProps.disabled = 'disabled';
			}

			return _react2.default.createElement('textarea', textareaProps);
		}
	}, {
		key: 'validate',
		value: function validate(value) {
			return this.props.validator ? this.props.validator(value) : value;
		}
	}]);

	return Textarea;
}(_react2.default.Component);

exports.default = Textarea;