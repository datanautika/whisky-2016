'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Router = require('../libs/Router');

var _Router2 = _interopRequireDefault(_Router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = new _Router2.default();