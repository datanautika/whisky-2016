import React from 'react';
import _ from 'lodash';

import styles from './Breadcrumbs.css';
import BrowserRouter from '../libs/BrowserRouter';
import router from '../streams/router';
import routeStream from '../streams/routeStream';
import whiskyName from '../utils/whiskyName';
import {whiskies, categories} from '../data';
import href from '../utils/href';


const DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

let isUrlExternal = (url) => {
	let testedDomain = DOMAIN_REGEX.exec(url);
	let localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

let browserRouter = new BrowserRouter(router);

export default class Breadcrumbs extends React.PureComponent {
	render() {
		let {page, subpage} = routeStream.get();
		let level1;
		let level2;

		if (page === 'uzivatele') {
			level1 = 'Uživatelé';
			level2 = subpage;
		}

		if (page === 'whisky') {
			level1 = 'Whisky';

			if (subpage) {
				let whisky = _.find(whiskies, {id: subpage});

				if (whisky) {
					level2 = whiskyName(whisky).join(' ');
				}
			}
		}

		if (page === 'kategorie') {
			level1 = 'Kategorie';

			if (subpage) {
				level2 = _.find(categories, {id: subpage}).name;
			}
		}

		return <nav className={styles.root} onClick={this.handleClick}>
			<ol>
				<li><a href={href('')}>◊</a></li>
				{level1 ? <li><a href={href(page)}>{level1}</a></li> : null}
				{level2 && (typeof subpage === 'number' || page !== 'whisky') ? <li>{level2}</li> : null}
			</ol>
		</nav>;
	}

	componentDidMount() {
		routeStream.subscribe(() => {
			this.forceUpdate();
		});
	}

	handleClick = (event) => {
		if (event.button !== 1) {
			let url = event.target.getAttribute('href');

			if (url && !DOMAIN_REGEX.test(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			} else if (url && !isUrlExternal(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			}
		}
	};
}
