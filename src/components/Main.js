import React from 'react';
import _ from 'lodash';

import styles from './Main.css';
import User from './User';
import WhiskyList from './WhiskyList';
import WhiskyDetail from './WhiskyDetail';
import WhiskyName from './WhiskyName';
import Category from './Category';
import {users, whiskies, categories} from '../data';
import BrowserRouter from '../libs/BrowserRouter';
import router from '../streams/router';
import routeStream from '../streams/routeStream';
import whiskyName from '../utils/whiskyName';
import href from '../utils/href';


// const MAILTO_REGEX = /^mailto:/;
// const ROUTE_LINK_REGEX = /^\//;
const DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

let isUrlExternal = (url) => {
	let testedDomain = DOMAIN_REGEX.exec(url);
	let localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

let browserRouter = new BrowserRouter(router);

let userList = () => <section className={styles.users}>
	<h2 className={styles.heading}><a href={href('uzivatele')}>Uživatelé</a></h2>
	<ul>
		{users.sort((a, b) => a.name.trim().localeCompare(b.name.trim())).map((user, index) => <li key={index}><a href={href('uzivatele', user.name)}>{user.name}</a></li>)}
	</ul>
</section>;

let whiskyList = () => <section className={styles.whiskies}>
	<h2 className={styles.heading}><a href={href('whisky')}>Whisky</a></h2>
	<ul>
		{whiskies.sort((a, b) => {
			let [titlePart1A, titlePart2A, subtitleA] = whiskyName(a);
			let [titlePart1B, titlePart2B, subtitleB] = whiskyName(b);
			
			if (!titlePart1A) {
				titlePart1A = titlePart2A;
			}

			if (!titlePart1B) {
				titlePart1B = titlePart2B;
			}

			let result = titlePart1A.trim().localeCompare(titlePart1B.trim());

			if (result !== 0) {
				return result;
			}

			if (a.age && b.age) {
				return a.age - b.age;
			}

			return `${titlePart2A} ${subtitleA}`.trim().localeCompare(`${titlePart2B} ${subtitleB}`.trim());
		}).map((whisky, index) => <li key={index}><WhiskyName key={index} data={whisky} /></li>)}
	</ul>
</section>;

let categoriesList = () => <section className={styles.categories}>
	<h2 className={styles.heading}><a href={href('kategorie')}>Kategorie</a></h2>
	<ul>
		{categories.map((category, index) => <li key={index}><a href={href('kategorie', category.id)}>{category.name}</a></li>)}
	</ul>
</section>;

export default class Main extends React.Component {
	render() {
		let {page, subpage} = routeStream.get();
		let elements = null;

		if (!page) {
			elements = <div className={styles.columns}>
				<section className={styles.help}>
					<h2 className={styles.heading}>Nápověda</h2>
					<p>Whisky jsou barevně označené podle oblasti a typu:</p>
					<ul className={styles.whiskyLabels}>
						<li><span className={`${styles.whiskyLabel} ${styles.islay}`}>Islay</span></li>
						<li><span className={`${styles.whiskyLabel} ${styles.islay} ${styles.isSingleMalt}`}>Islay, single malt</span></li>
						<li><span className={`${styles.whiskyLabel} ${styles.islands}`}>Ostrovy</span></li>
						<li><span className={`${styles.whiskyLabel} ${styles.islands} ${styles.isSingleMalt}`}>Ostrovy, single malt</span></li>
						<li><span className={`${styles.whiskyLabel} ${styles.lowland}`}>Nížina</span></li>
						<li><span className={`${styles.whiskyLabel} ${styles.lowland} ${styles.isSingleMalt}`}>Nížina, single malt</span></li>
						<li><span className={`${styles.whiskyLabel} ${styles.highland}`}>Vysočina</span></li>
						<li><span className={`${styles.whiskyLabel} ${styles.highland} ${styles.isSingleMalt}`}>Vysočina, single malt</span></li>
						<li><span className={`${styles.whiskyLabel} ${styles.speyside}`}>Speyside</span></li>
						<li><span className={`${styles.whiskyLabel} ${styles.speyside} ${styles.isSingleMalt}`}>Speyside, single malt</span></li>
						<li><span className={`${styles.whiskyLabel} ${styles.campbeltown}`}>Campbeltown</span></li>
						<li><span className={`${styles.whiskyLabel} ${styles.campbeltown} ${styles.isSingleMalt}`}>Campbeltown, single malt</span></li>
						<li><span className={`${styles.whiskyLabel}`}>Jiná oblast</span></li>
						<li><span className={`${styles.whiskyLabel} ${styles.isSingleMalt}`}>Jiná oblast, single malt</span></li>
					</ul>
				</section>

				{userList()}
				{categoriesList()}
				{whiskyList()}
			</div>;
		}

		if (page === 'uzivatele' && !subpage) {
			elements = userList();
		}

		if (page === 'uzivatele' && subpage) {
			elements = <User data={_.find(users, {name: subpage})} />;
		}

		if (page === 'whisky' && typeof subpage !== 'number') {
			elements = <WhiskyList />;
		}

		if (page === 'whisky' && typeof subpage === 'number') {
			elements = <WhiskyDetail data={_.find(whiskies, {id: subpage})} />;
		}

		if (page === 'kategorie' && !subpage) {
			elements = categoriesList();
		}

		if (page === 'kategorie' && typeof subpage === 'number') {
			elements = <Category data={_.find(categories, {id: subpage})} />;
		}

		return <main className={styles.root} onClick={this.handleClick}>
			{elements}
		</main>;
	}

	componentDidMount() {
		routeStream.subscribe(() => {
			this.forceUpdate();
		});
	}

	handleClick = (event) => {
		if (event.button !== 1) {
			let url = event.target.getAttribute('href');

			if (url && !DOMAIN_REGEX.test(url)) {
				event.preventDefault();

				if (/whisky\/.+\//.test(url)) {
					browserRouter.navigate(url, {resetScrollPosition: false});
				} else {
					browserRouter.navigate(url);
				}
			} else if (url && !isUrlExternal(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			}
		}

		/*if (!(this.props.link || MAILTO_REGEX.test(this.props.link))) {
			event.preventDefault();

			if (this.props.handleClick) {
				this.props.handleClick();
			}
		} else if (ROUTE_LINK_REGEX.test(this.props.link) && this.props.useRouter !== false) {
			event.preventDefault();

			browserRouter.navigate(this.props.link);
		}*/
	};
}
