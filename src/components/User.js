import React from 'react';
import _ from 'lodash';

import styles from './User.css';
import WhiskyName from './WhiskyName';
import href from '../utils/href';
import {whiskies, categories} from '../data';
import BrowserRouter from '../libs/BrowserRouter';
import router from '../streams/router';


const DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

let isUrlExternal = (url) => {
	let testedDomain = DOMAIN_REGEX.exec(url);
	let localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

let browserRouter = new BrowserRouter(router);

export default class User extends React.Component {
	render() {
		let user = this.props.data;

		let categoriesElements = user.results.map((result, resultIndex) => {
			let itemsElements = result.items.map((item, itemIndex) => {
				if (item && typeof item === 'number') {
					return <li key={itemIndex} className={styles.categoryItem}><WhiskyName key={itemIndex} data={_.find(whiskies, {id: item})} /></li>;
				} else if (item && typeof item === 'object' && typeof item.id === 'number') {
					return <li key={itemIndex} className={styles.categoryItem}><WhiskyName key={itemIndex} data={_.find(whiskies, {id: item.id})} /><p className={styles.categoryItemDescription}>{item.description}</p></li>;
				} else if (item && typeof item === 'object' && (typeof item.name === 'string' || typeof item.description === 'string')) {
					return <li key={itemIndex} className={styles.categoryItem}>
						<p className={styles.categoryItemName}>{item.name}</p>
						<p className={styles.categoryItemDescription}>{item.description}</p>
						{item.links && item.links.length ? <ul className={styles.categoryItemLinks}>{item.links.map((link, index) => <li key={index}><a href={link}>{link}</a></li>)}</ul> : null}
					</li>;
				}

				return <li key={itemIndex} className={`${styles.categoryItem} ${styles.isEmpty}`}><p>–</p></li>;
			});

			return <section className={styles.category} key={resultIndex} onClick={this.handleClick}>
				<h3 className={styles.categoryHeading}><a href={href('kategorie', _.find(categories, {name: result.categoryName}).id)}>{result.categoryName}</a></h3>
				{itemsElements && itemsElements.length ? <ol className={styles.categoryResults}>{itemsElements}</ol> : <p>–</p>}
			</section>;
		});

		return <div className={styles.root}>
			<h2 className={styles.nameHeading}>{`${this.props.data.name}`}</h2>
			{categoriesElements}
		</div>;
	}

	handleClick = (event) => {
		if (event.button !== 1) {
			let url = event.target.getAttribute('href');

			if (url && !DOMAIN_REGEX.test(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			} else if (url && !isUrlExternal(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			}
		}
	};
}
