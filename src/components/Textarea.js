import React from 'react';

import styles from './Textarea.css';


export default class Textarea extends React.Component {
	shouldComponentUpdate(newProps) {
		return newProps.id !== this.props.id ||
			newProps.name !== this.props.name ||
			newProps.rows !== this.props.rows ||
			newProps.isValid !== this.props.isValid ||
			newProps.isInvalid !== this.props.isInvalid ||
			newProps.isDisabled !== this.props.isDisabled ||
			newProps.handleChange !== this.props.handleChange ||
			newProps.handleSave !== this.props.handleSave ||
			newProps.validator !== this.props.validator ||
			newProps.value !== this.props.value;
	}

	render() {
		let textareaProps = {
			key: this.props.id || this.props.name || '',
			className: styles.default + (this.props.isInvalid ? ' isInvalid' : '') + (this.props.isValid ? ' isValid' : '') + (this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
			name: this.props.name || this.props.id || '',
			id: this.props.id || this.props.name || '',
			rows: this.props.rows || 5,
			onBlur: this.handleFocusOut,
			onChange: this.handleInput,
			value: this.props.value
		};

		if (this.props.isDisabled) {
			textareaProps.disabled = 'disabled';
		}

		return <textarea {...textareaProps} />;
	}

	handleInput = (event) => {
		if (this.props.handleChange) {
			this.props.handleChange(this.validate(event.target.value));
		}
	};

	handleFocusOut = (event) => {
		if (this.props.handleSave) {
			this.props.handleSave(this.validate(event.target.value));
		}
	};

	validate(value) {
		return this.props.validator ? this.props.validator(value) : value;
	}
}
