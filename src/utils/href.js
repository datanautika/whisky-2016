export default function href(...levels) {
	return `${process.env.APP_ROOT ? `${process.env.APP_ROOT}` : ''}/${levels.join('/')}`;
}
