/* eslint-disable quote-props */

import constants from '../../internals/constants';


const EN = constants.EN;
const CZ = constants.CZ;


let i18nStrings = {
	// ModelsList
	'ModelsList Heading': {
		[EN]: 'Models',
		[CZ]: 'Modely'
	},

	// NotesList
	'NotesList Heading': {
		[EN]: 'Notes',
		[CZ]: 'Poznámky'
	},
	'NotesList Note Name {0}': {
		[EN]: 'Note #{0}',
		[CZ]: 'Poznámka #{0}'
	},
	'NotesList AddButton': {
		[EN]: 'Add note',
		[CZ]: 'Přidat poznámku'
	},
	'NotesList DoneButton': {
		[EN]: 'Done',
		[CZ]: 'Hotovo'
	},
	'NotesList NoteCaption': {
		[EN]: 'Note:',
		[CZ]: 'Poznámka:'
	},

	// notifications
	'Model loaded Notification Title': {
		[EN]: 'Models loaded',
		[CZ]: 'Modely načteny'
	},
	'Case data loaded Notification Title': {
		[EN]: 'Case data loaded',
		[CZ]: 'Data o případu načtena'
	},
	'Case data loaded Notification Text {0}': {
		[EN]: 'Case {0}',
		[CZ]: 'Případ {0}'
	}
};

export default i18nStrings;
