import nconf from 'nconf';
// import path from 'path';

import secrets from './secrets';
// import appRoot from '../../internals/appRoot';


nconf.argv();
nconf.env();
// nconf.use('file', {file: path.join(appRoot, 'private/credentials.json')});

nconf.add('secrets', {type: 'literal', store: {secrets}});

export default nconf;
