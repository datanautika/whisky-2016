'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _User = require('./User.css');

var _User2 = _interopRequireDefault(_User);

var _WhiskyName = require('./WhiskyName');

var _WhiskyName2 = _interopRequireDefault(_WhiskyName);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _data = require('../data');

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

var isUrlExternal = function isUrlExternal(url) {
	var testedDomain = DOMAIN_REGEX.exec(url);
	var localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

var browserRouter = new _BrowserRouter2.default(_router2.default);

var User = function (_React$Component) {
	(0, _inherits3.default)(User, _React$Component);

	function User() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, User);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = User.__proto__ || (0, _getPrototypeOf2.default)(User)).call.apply(_ref, [this].concat(args))), _this), _this.handleClick = function (event) {
			if (event.button !== 1) {
				var url = event.target.getAttribute('href');

				if (url && !DOMAIN_REGEX.test(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				} else if (url && !isUrlExternal(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				}
			}
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(User, [{
		key: 'render',
		value: function render() {
			var _this2 = this;

			var user = this.props.data;

			var categoriesElements = user.results.map(function (result, resultIndex) {
				var itemsElements = result.items.map(function (item, itemIndex) {
					if (item && typeof item === 'number') {
						return _react2.default.createElement(
							'li',
							{ key: itemIndex, className: _User2.default.categoryItem },
							_react2.default.createElement(_WhiskyName2.default, { key: itemIndex, data: _lodash2.default.find(_data.whiskies, { id: item }) })
						);
					} else if (item && typeof item === 'object' && typeof item.id === 'number') {
						return _react2.default.createElement(
							'li',
							{ key: itemIndex, className: _User2.default.categoryItem },
							_react2.default.createElement(_WhiskyName2.default, { key: itemIndex, data: _lodash2.default.find(_data.whiskies, { id: item.id }) }),
							_react2.default.createElement(
								'p',
								{ className: _User2.default.categoryItemDescription },
								item.description
							)
						);
					} else if (item && typeof item === 'object' && (typeof item.name === 'string' || typeof item.description === 'string')) {
						return _react2.default.createElement(
							'li',
							{ key: itemIndex, className: _User2.default.categoryItem },
							_react2.default.createElement(
								'p',
								{ className: _User2.default.categoryItemName },
								item.name
							),
							_react2.default.createElement(
								'p',
								{ className: _User2.default.categoryItemDescription },
								item.description
							),
							item.links && item.links.length ? _react2.default.createElement(
								'ul',
								{ className: _User2.default.categoryItemLinks },
								item.links.map(function (link, index) {
									return _react2.default.createElement(
										'li',
										{ key: index },
										_react2.default.createElement(
											'a',
											{ href: link },
											link
										)
									);
								})
							) : null
						);
					}

					return _react2.default.createElement(
						'li',
						{ key: itemIndex, className: _User2.default.categoryItem + ' ' + _User2.default.isEmpty },
						_react2.default.createElement(
							'p',
							null,
							'\u2013'
						)
					);
				});

				return _react2.default.createElement(
					'section',
					{ className: _User2.default.category, key: resultIndex, onClick: _this2.handleClick },
					_react2.default.createElement(
						'h3',
						{ className: _User2.default.categoryHeading },
						_react2.default.createElement(
							'a',
							{ href: (0, _href2.default)('kategorie', _lodash2.default.find(_data.categories, { name: result.categoryName }).id) },
							result.categoryName
						)
					),
					itemsElements && itemsElements.length ? _react2.default.createElement(
						'ol',
						{ className: _User2.default.categoryResults },
						itemsElements
					) : _react2.default.createElement(
						'p',
						null,
						'\u2013'
					)
				);
			});

			return _react2.default.createElement(
				'div',
				{ className: _User2.default.root },
				_react2.default.createElement(
					'h2',
					{ className: _User2.default.nameHeading },
					'' + this.props.data.name
				),
				categoriesElements
			);
		}
	}]);
	return User;
}(_react2.default.Component);

exports.default = User;