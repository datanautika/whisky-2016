'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _WhiskyName = require('./WhiskyName.css');

var _WhiskyName2 = _interopRequireDefault(_WhiskyName);

var _whiskyName3 = require('../utils/whiskyName');

var _whiskyName4 = _interopRequireDefault(_whiskyName3);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

var isUrlExternal = function isUrlExternal(url) {
	var testedDomain = DOMAIN_REGEX.exec(url);
	var localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

var browserRouter = new _BrowserRouter2.default(_router2.default);

var WhiskyName = function (_React$Component) {
	(0, _inherits3.default)(WhiskyName, _React$Component);

	function WhiskyName() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, WhiskyName);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = WhiskyName.__proto__ || (0, _getPrototypeOf2.default)(WhiskyName)).call.apply(_ref, [this].concat(args))), _this), _this.handleClick = function (event) {
			if (event.button !== 1) {
				var url = _this.refs.root.getAttribute('href');

				if (url && !DOMAIN_REGEX.test(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				} else if (url && !isUrlExternal(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				}
			}
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(WhiskyName, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(newProps) {
			var shouldUpdate = !this.props.data.links && !newProps.data.links || this.props.data.links && newProps.data.links && this.props.data.links[0] !== newProps.data.links[0] || this.props.data.age !== newProps.data.age || this.props.data.batch !== newProps.data.batch || this.props.data.bottled !== newProps.data.bottled || this.props.data.bottledFor !== newProps.data.bottledFor || this.props.data.bottler !== newProps.data.bottler || this.props.data.bottles !== newProps.data.bottles || this.props.data.brand !== newProps.data.brand || this.props.data.caskNumber !== newProps.data.caskNumber || this.props.data.caskType !== newProps.data.caskType ||
			// this.props.data.categoryGroup !== newProps.data.categoryGroup ||
			// this.props.data.categoryName !== newProps.data.categoryName ||
			this.props.data.country !== newProps.data.country || this.props.data.distillery !== newProps.data.distillery || this.props.data.edition !== newProps.data.edition || this.props.data.id !== newProps.data.id ||
			// this.props.data.image !== newProps.data.image ||
			this.props.data.isCaskStrength !== newProps.data.isCaskStrength || this.props.data.isNonChillfiltered !== newProps.data.isNonChillfiltered || this.props.data.isSingleCask !== newProps.data.isSingleCask || this.props.data.isUncolored !== newProps.data.isUncolored || this.props.data.name !== newProps.data.name ||
			// this.props.data.pointsCount !== newProps.data.pointsCount ||
			this.props.data.region !== newProps.data.region || this.props.data.size !== newProps.data.size || this.props.data.strength !== newProps.data.strength || this.props.data.type !== newProps.data.type ||
			// this.props.data.userId !== newProps.data.userId ||
			this.props.data.vintage !== newProps.data.vintage;

			return shouldUpdate;
		}
	}, {
		key: 'render',
		value: function render() {
			var whisky = this.props.data;
			var isSingleMalt = whisky.type === 'Single Malt Scotch' || whisky.type === 'Single Malt Whisky' || whisky.type === 'Single Malt Whiskey';

			if (!whisky) {
				return null;
			}

			var _whiskyName = (0, _whiskyName4.default)(whisky),
			    _whiskyName2 = (0, _slicedToArray3.default)(_whiskyName, 3),
			    titlePart1 = _whiskyName2[0],
			    titlePart2 = _whiskyName2[1],
			    subtitle = _whiskyName2[2];

			var regionClass = void 0;

			if (titlePart2 && subtitle) {
				titlePart2 = titlePart2 + ' ';
			}

			if (whisky.region) {
				regionClass = _WhiskyName2.default[whisky.region.toLowerCase()];
			}

			return _react2.default.createElement(
				'a',
				{ ref: 'root', className: _WhiskyName2.default.root + (regionClass ? ' ' + regionClass : '') + (isSingleMalt ? ' ' + _WhiskyName2.default.isSingleMalt : ''), href: (0, _href2.default)('whisky', whisky.id), onClick: this.handleClick },
				_react2.default.createElement(
					'span',
					{ className: _WhiskyName2.default.title },
					titlePart1 ? titlePart1 : null,
					titlePart2 ? _react2.default.createElement(
						'b',
						null,
						titlePart2
					) : null
				),
				_react2.default.createElement(
					'span',
					{ className: _WhiskyName2.default.subtitle },
					subtitle ? subtitle : null
				)
			);
		}
	}]);
	return WhiskyName;
}(_react2.default.Component);

exports.default = WhiskyName;