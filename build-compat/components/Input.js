'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Input = require('./Input.css');

var _Input2 = _interopRequireDefault(_Input);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ENTER_KEY_CODE = 13;

var Input = function (_React$Component) {
	(0, _inherits3.default)(Input, _React$Component);

	function Input() {
		(0, _classCallCheck3.default)(this, Input);
		return (0, _possibleConstructorReturn3.default)(this, (Input.__proto__ || (0, _getPrototypeOf2.default)(Input)).apply(this, arguments));
	}

	(0, _createClass3.default)(Input, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(newProps) {
			return newProps.id !== this.props.id || newProps.name !== this.props.name || newProps.type !== this.props.type || newProps.autocomplete !== this.props.autocomplete || newProps.isValid !== this.props.isValid || newProps.isInvalid !== this.props.isInvalid || newProps.isDisabled !== this.props.isDisabled || newProps.handleChange !== this.props.handleChange || newProps.handleSave !== this.props.handleSave || newProps.validator !== this.props.validator;
		}
	}, {
		key: 'render',
		value: function render() {
			var inputProps = {
				key: this.props.id || this.props.name || '',
				className: _Input2.default.default + (this.props.isValid ? ' isValid' : '') + (this.props.isInvalid ? ' isInvalid' : '') + (this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
				type: 'text',
				name: this.props.name || this.props.id || '',
				id: this.props.id || this.props.name || '',
				onBlur: this.handleFocusOut,
				onChange: this.handleInput,
				onIeydown: this.handleKeyDown
			};

			if (this.props.type === 'email') {
				inputProps.type = this.props.type;
			}

			if (this.props.isDisabled) {
				inputProps.disabled = 'disabled';
			}

			if (this.props.autocomplete === false) {
				inputProps.autocomplete = 'off';
			}

			return _react2.default.createElement('input', inputProps);
		}
	}, {
		key: 'handleInput',
		value: function handleInput(event) {
			if (this.props.handleChange) {
				this.props.handleChange(this.validate(event.target.value));
			}
		}
	}, {
		key: 'handleFocusOut',
		value: function handleFocusOut(event) {
			if (this.props.handleSave) {
				this.props.handleSave(this.validate(event.target.value));
			}
		}
	}, {
		key: 'handleKeyDown',
		value: function handleKeyDown(event) {
			if (event.keyCode === ENTER_KEY_CODE) {
				if (this.props.handleSave) {
					this.props.handleSave(this.validate(event.target.value));
				}
			}
		}
	}, {
		key: 'validate',
		value: function validate(value) {
			return this.props.validator ? this.props.validator(value) : value;
		}
	}]);
	return Input;
}(_react2.default.Component);

exports.default = Input;