'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _Breadcrumbs = require('./Breadcrumbs.css');

var _Breadcrumbs2 = _interopRequireDefault(_Breadcrumbs);

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

var _routeStream = require('../streams/routeStream');

var _routeStream2 = _interopRequireDefault(_routeStream);

var _whiskyName = require('../utils/whiskyName');

var _whiskyName2 = _interopRequireDefault(_whiskyName);

var _data = require('../data');

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

var isUrlExternal = function isUrlExternal(url) {
	var testedDomain = DOMAIN_REGEX.exec(url);
	var localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

var browserRouter = new _BrowserRouter2.default(_router2.default);

var Breadcrumbs = function (_React$PureComponent) {
	(0, _inherits3.default)(Breadcrumbs, _React$PureComponent);

	function Breadcrumbs() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, Breadcrumbs);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Breadcrumbs.__proto__ || (0, _getPrototypeOf2.default)(Breadcrumbs)).call.apply(_ref, [this].concat(args))), _this), _this.handleClick = function (event) {
			if (event.button !== 1) {
				var url = event.target.getAttribute('href');

				if (url && !DOMAIN_REGEX.test(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				} else if (url && !isUrlExternal(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				}
			}
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(Breadcrumbs, [{
		key: 'render',
		value: function render() {
			var _routeStream$get = _routeStream2.default.get(),
			    page = _routeStream$get.page,
			    subpage = _routeStream$get.subpage;

			var level1 = void 0;
			var level2 = void 0;

			if (page === 'uzivatele') {
				level1 = 'Uživatelé';
				level2 = subpage;
			}

			if (page === 'whisky') {
				level1 = 'Whisky';

				if (subpage) {
					var whisky = _lodash2.default.find(_data.whiskies, { id: subpage });

					if (whisky) {
						level2 = (0, _whiskyName2.default)(whisky).join(' ');
					}
				}
			}

			if (page === 'kategorie') {
				level1 = 'Kategorie';

				if (subpage) {
					level2 = _lodash2.default.find(_data.categories, { id: subpage }).name;
				}
			}

			return _react2.default.createElement(
				'nav',
				{ className: _Breadcrumbs2.default.root, onClick: this.handleClick },
				_react2.default.createElement(
					'ol',
					null,
					_react2.default.createElement(
						'li',
						null,
						_react2.default.createElement(
							'a',
							{ href: (0, _href2.default)('') },
							'\u25CA'
						)
					),
					level1 ? _react2.default.createElement(
						'li',
						null,
						_react2.default.createElement(
							'a',
							{ href: (0, _href2.default)(page) },
							level1
						)
					) : null,
					level2 && (typeof subpage === 'number' || page !== 'whisky') ? _react2.default.createElement(
						'li',
						null,
						level2
					) : null
				)
			);
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _this2 = this;

			_routeStream2.default.subscribe(function () {
				_this2.forceUpdate();
			});
		}
	}]);
	return Breadcrumbs;
}(_react2.default.PureComponent);

exports.default = Breadcrumbs;