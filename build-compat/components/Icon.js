'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Icon = require('./Icon.css');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var OLD_EDGE_ID = 10547;
var OLD_WEBKIT_ID = 537;

function embed(svg, target) {
	// if the target exists
	if (target) {
		// create a document fragment to hold the contents of the target
		var fragment = document.createDocumentFragment();

		// cache the closest matching viewBox
		var viewBox = !svg.getAttribute('viewBox') && target.getAttribute('viewBox');

		// conditionally set the viewBox on the svg
		if (viewBox) {
			svg.setAttribute('viewBox', viewBox);
		}

		// clone the target
		var clone = target.cloneNode(true);

		// copy the contents of the clone into the fragment
		while (clone.childNodes.length) {
			fragment.appendChild(clone.firstChild);
		}

		// append the fragment into the svg
		svg.appendChild(fragment);
	}
}

function loadReadyStateChange(xhr) {
	// listen to changes in the request
	xhr.onreadystatechange = function () {
		// if the request is ready
		if (xhr.readyState === 4) {
			(function () {
				// get the cached html document
				var cachedDocument = xhr._cachedDocument;

				// ensure the cached html document based on the xhr response
				if (!cachedDocument) {
					cachedDocument = xhr._cachedDocument = document.implementation.createHTMLDocument('');

					cachedDocument.body.innerHTML = xhr.responseText;

					xhr._cachedTarget = {};
				}

				// clear the xhr embeds list and embed each item
				xhr._embeds.splice(0).map(function (item) {
					// get the cached target
					var target = xhr._cachedTarget[item.id];

					// ensure the cached target
					if (!target) {
						target = xhr._cachedTarget[item.id] = cachedDocument.getElementById(item.id);
					}

					// embed the target into the svg
					embed(item.svg, target);
				});
			})();
		}
	};

	// test the ready state change immediately
	xhr.onreadystatechange();
}

var Icon = function (_React$Component) {
	(0, _inherits3.default)(Icon, _React$Component);

	function Icon() {
		(0, _classCallCheck3.default)(this, Icon);
		return (0, _possibleConstructorReturn3.default)(this, (Icon.__proto__ || (0, _getPrototypeOf2.default)(Icon)).apply(this, arguments));
	}

	(0, _createClass3.default)(Icon, [{
		key: 'render',
		value: function render() {
			var styleSuffix = '';
			var idSuffix = '';

			if (this.props.size === 'medium') {
				styleSuffix = '--medium';
				idSuffix = '-medium';
			} else if (this.props.size === 'large') {
				styleSuffix = '--large';
				idSuffix = '-large';
			}

			return _react2.default.createElement(
				'span',
				{ ref: 'root', className: _Icon2.default['root' + styleSuffix] },
				_react2.default.createElement(
					'svg',
					{ xmlns: 'http://www.w3.org/2000/svg' },
					_react2.default.createElement('use', { ref: 'use', xlinkHref: './assets/icons.svg#' + (this.props.id + idSuffix) })
				)
			);
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			var domNode = this.refs.root;

			if (!domNode) {
				return;
			}

			var useNode = this.refs.use;

			// set whether the polyfill will be activated or not
			var polyfill = void 0;
			var newerIEUA = /\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/;
			var webkitUA = /\bAppleWebKit\/(\d+)\b/;
			var olderEdgeUA = /\bEdge\/12\.(\d+)\b/;

			polyfill = newerIEUA.test(navigator.userAgent) || (navigator.userAgent.match(olderEdgeUA) || [])[1] < OLD_EDGE_ID || (navigator.userAgent.match(webkitUA) || [])[1] < OLD_WEBKIT_ID;

			// create xhr requests object
			var requests = {};

			if (polyfill) {
				requestAnimationFrame(function () {
					// get the current <svg>
					var svg = useNode.parentNode;

					if (svg && /svg/i.test(svg.nodeName)) {
						var src = useNode.getAttribute('xlink:href') || useNode.getAttribute('href');

						// remove the <use> element
						svg.removeChild(useNode);

						// parse the src and get the url and id
						var srcSplit = src.split('#');
						var url = srcSplit.shift();
						var id = srcSplit.join('#');

						// if the link is external
						if (url.length) {
							// get the cached xhr request
							var xhr = requests[url];

							// ensure the xhr request exists
							if (!xhr) {
								xhr = requests[url] = new XMLHttpRequest();

								xhr.open('GET', url);
								xhr.send();

								xhr._embeds = [];
							}

							// add the svg and id as an item to the xhr embeds list
							xhr._embeds.push({
								svg: svg,
								id: id
							});

							// prepare the xhr ready state change event
							loadReadyStateChange(xhr);
						} else {
							// embed the local id into the svg
							embed(svg, document.getElementById(id));
						}
					}
				});
			}
		}
	}]);
	return Icon;
}(_react2.default.Component);

exports.default = Icon;