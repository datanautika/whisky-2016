'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Textarea = require('./Textarea.css');

var _Textarea2 = _interopRequireDefault(_Textarea);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Textarea = function (_React$Component) {
	(0, _inherits3.default)(Textarea, _React$Component);

	function Textarea() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, Textarea);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Textarea.__proto__ || (0, _getPrototypeOf2.default)(Textarea)).call.apply(_ref, [this].concat(args))), _this), _this.handleInput = function (event) {
			if (_this.props.handleChange) {
				_this.props.handleChange(_this.validate(event.target.value));
			}
		}, _this.handleFocusOut = function (event) {
			if (_this.props.handleSave) {
				_this.props.handleSave(_this.validate(event.target.value));
			}
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(Textarea, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(newProps) {
			return newProps.id !== this.props.id || newProps.name !== this.props.name || newProps.rows !== this.props.rows || newProps.isValid !== this.props.isValid || newProps.isInvalid !== this.props.isInvalid || newProps.isDisabled !== this.props.isDisabled || newProps.handleChange !== this.props.handleChange || newProps.handleSave !== this.props.handleSave || newProps.validator !== this.props.validator || newProps.value !== this.props.value;
		}
	}, {
		key: 'render',
		value: function render() {
			var textareaProps = {
				key: this.props.id || this.props.name || '',
				className: _Textarea2.default.default + (this.props.isInvalid ? ' isInvalid' : '') + (this.props.isValid ? ' isValid' : '') + (this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
				name: this.props.name || this.props.id || '',
				id: this.props.id || this.props.name || '',
				rows: this.props.rows || 5,
				onBlur: this.handleFocusOut,
				onChange: this.handleInput,
				value: this.props.value
			};

			if (this.props.isDisabled) {
				textareaProps.disabled = 'disabled';
			}

			return _react2.default.createElement('textarea', textareaProps);
		}
	}, {
		key: 'validate',
		value: function validate(value) {
			return this.props.validator ? this.props.validator(value) : value;
		}
	}]);
	return Textarea;
}(_react2.default.Component);

exports.default = Textarea;