'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _Main = require('./Main.css');

var _Main2 = _interopRequireDefault(_Main);

var _User = require('./User');

var _User2 = _interopRequireDefault(_User);

var _WhiskyList = require('./WhiskyList');

var _WhiskyList2 = _interopRequireDefault(_WhiskyList);

var _WhiskyDetail = require('./WhiskyDetail');

var _WhiskyDetail2 = _interopRequireDefault(_WhiskyDetail);

var _WhiskyName = require('./WhiskyName');

var _WhiskyName2 = _interopRequireDefault(_WhiskyName);

var _Category = require('./Category');

var _Category2 = _interopRequireDefault(_Category);

var _data = require('../data');

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

var _routeStream = require('../streams/routeStream');

var _routeStream2 = _interopRequireDefault(_routeStream);

var _whiskyName5 = require('../utils/whiskyName');

var _whiskyName6 = _interopRequireDefault(_whiskyName5);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// const MAILTO_REGEX = /^mailto:/;
// const ROUTE_LINK_REGEX = /^\//;
var DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

var isUrlExternal = function isUrlExternal(url) {
	var testedDomain = DOMAIN_REGEX.exec(url);
	var localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

var browserRouter = new _BrowserRouter2.default(_router2.default);

var userList = function userList() {
	return _react2.default.createElement(
		'section',
		{ className: _Main2.default.users },
		_react2.default.createElement(
			'h2',
			{ className: _Main2.default.heading },
			_react2.default.createElement(
				'a',
				{ href: (0, _href2.default)('uzivatele') },
				'U\u017Eivatel\xE9'
			)
		),
		_react2.default.createElement(
			'ul',
			null,
			_data.users.sort(function (a, b) {
				return a.name.trim().localeCompare(b.name.trim());
			}).map(function (user, index) {
				return _react2.default.createElement(
					'li',
					{ key: index },
					_react2.default.createElement(
						'a',
						{ href: (0, _href2.default)('uzivatele', user.name) },
						user.name
					)
				);
			})
		)
	);
};

var whiskyList = function whiskyList() {
	return _react2.default.createElement(
		'section',
		{ className: _Main2.default.whiskies },
		_react2.default.createElement(
			'h2',
			{ className: _Main2.default.heading },
			_react2.default.createElement(
				'a',
				{ href: (0, _href2.default)('whisky') },
				'Whisky'
			)
		),
		_react2.default.createElement(
			'ul',
			null,
			_data.whiskies.sort(function (a, b) {
				var _whiskyName = (0, _whiskyName6.default)(a),
				    _whiskyName2 = (0, _slicedToArray3.default)(_whiskyName, 3),
				    titlePart1A = _whiskyName2[0],
				    titlePart2A = _whiskyName2[1],
				    subtitleA = _whiskyName2[2];

				var _whiskyName3 = (0, _whiskyName6.default)(b),
				    _whiskyName4 = (0, _slicedToArray3.default)(_whiskyName3, 3),
				    titlePart1B = _whiskyName4[0],
				    titlePart2B = _whiskyName4[1],
				    subtitleB = _whiskyName4[2];

				if (!titlePart1A) {
					titlePart1A = titlePart2A;
				}

				if (!titlePart1B) {
					titlePart1B = titlePart2B;
				}

				var result = titlePart1A.trim().localeCompare(titlePart1B.trim());

				if (result !== 0) {
					return result;
				}

				if (a.age && b.age) {
					return a.age - b.age;
				}

				return (titlePart2A + ' ' + subtitleA).trim().localeCompare((titlePart2B + ' ' + subtitleB).trim());
			}).map(function (whisky, index) {
				return _react2.default.createElement(
					'li',
					{ key: index },
					_react2.default.createElement(_WhiskyName2.default, { key: index, data: whisky })
				);
			})
		)
	);
};

var categoriesList = function categoriesList() {
	return _react2.default.createElement(
		'section',
		{ className: _Main2.default.categories },
		_react2.default.createElement(
			'h2',
			{ className: _Main2.default.heading },
			_react2.default.createElement(
				'a',
				{ href: (0, _href2.default)('kategorie') },
				'Kategorie'
			)
		),
		_react2.default.createElement(
			'ul',
			null,
			_data.categories.map(function (category, index) {
				return _react2.default.createElement(
					'li',
					{ key: index },
					_react2.default.createElement(
						'a',
						{ href: (0, _href2.default)('kategorie', category.id) },
						category.name
					)
				);
			})
		)
	);
};

var Main = function (_React$Component) {
	(0, _inherits3.default)(Main, _React$Component);

	function Main() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, Main);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Main.__proto__ || (0, _getPrototypeOf2.default)(Main)).call.apply(_ref, [this].concat(args))), _this), _this.handleClick = function (event) {
			if (event.button !== 1) {
				var url = event.target.getAttribute('href');

				if (url && !DOMAIN_REGEX.test(url)) {
					event.preventDefault();

					if (/whisky\/.+\//.test(url)) {
						browserRouter.navigate(url, { resetScrollPosition: false });
					} else {
						browserRouter.navigate(url);
					}
				} else if (url && !isUrlExternal(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				}
			}

			/*if (!(this.props.link || MAILTO_REGEX.test(this.props.link))) {
   	event.preventDefault();
   		if (this.props.handleClick) {
   		this.props.handleClick();
   	}
   } else if (ROUTE_LINK_REGEX.test(this.props.link) && this.props.useRouter !== false) {
   	event.preventDefault();
   		browserRouter.navigate(this.props.link);
   }*/
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(Main, [{
		key: 'render',
		value: function render() {
			var _routeStream$get = _routeStream2.default.get(),
			    page = _routeStream$get.page,
			    subpage = _routeStream$get.subpage;

			var elements = null;

			if (!page) {
				elements = _react2.default.createElement(
					'div',
					{ className: _Main2.default.columns },
					_react2.default.createElement(
						'section',
						{ className: _Main2.default.help },
						_react2.default.createElement(
							'h2',
							{ className: _Main2.default.heading },
							'N\xE1pov\u011Bda'
						),
						_react2.default.createElement(
							'p',
							null,
							'Whisky jsou barevn\u011B ozna\u010Den\xE9 podle oblasti a typu:'
						),
						_react2.default.createElement(
							'ul',
							{ className: _Main2.default.whiskyLabels },
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: _Main2.default.whiskyLabel + ' ' + _Main2.default.islay },
									'Islay'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: _Main2.default.whiskyLabel + ' ' + _Main2.default.islay + ' ' + _Main2.default.isSingleMalt },
									'Islay, single malt'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: _Main2.default.whiskyLabel + ' ' + _Main2.default.islands },
									'Ostrovy'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: _Main2.default.whiskyLabel + ' ' + _Main2.default.islands + ' ' + _Main2.default.isSingleMalt },
									'Ostrovy, single malt'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: _Main2.default.whiskyLabel + ' ' + _Main2.default.lowland },
									'N\xED\u017Eina'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: _Main2.default.whiskyLabel + ' ' + _Main2.default.lowland + ' ' + _Main2.default.isSingleMalt },
									'N\xED\u017Eina, single malt'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: _Main2.default.whiskyLabel + ' ' + _Main2.default.highland },
									'Vyso\u010Dina'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: _Main2.default.whiskyLabel + ' ' + _Main2.default.highland + ' ' + _Main2.default.isSingleMalt },
									'Vyso\u010Dina, single malt'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: _Main2.default.whiskyLabel + ' ' + _Main2.default.speyside },
									'Speyside'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: _Main2.default.whiskyLabel + ' ' + _Main2.default.speyside + ' ' + _Main2.default.isSingleMalt },
									'Speyside, single malt'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: _Main2.default.whiskyLabel + ' ' + _Main2.default.campbeltown },
									'Campbeltown'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: _Main2.default.whiskyLabel + ' ' + _Main2.default.campbeltown + ' ' + _Main2.default.isSingleMalt },
									'Campbeltown, single malt'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: '' + _Main2.default.whiskyLabel },
									'Jin\xE1 oblast'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: _Main2.default.whiskyLabel + ' ' + _Main2.default.isSingleMalt },
									'Jin\xE1 oblast, single malt'
								)
							)
						)
					),
					userList(),
					categoriesList(),
					whiskyList()
				);
			}

			if (page === 'uzivatele' && !subpage) {
				elements = userList();
			}

			if (page === 'uzivatele' && subpage) {
				elements = _react2.default.createElement(_User2.default, { data: _lodash2.default.find(_data.users, { name: subpage }) });
			}

			if (page === 'whisky' && typeof subpage !== 'number') {
				elements = _react2.default.createElement(_WhiskyList2.default, null);
			}

			if (page === 'whisky' && typeof subpage === 'number') {
				elements = _react2.default.createElement(_WhiskyDetail2.default, { data: _lodash2.default.find(_data.whiskies, { id: subpage }) });
			}

			if (page === 'kategorie' && !subpage) {
				elements = categoriesList();
			}

			if (page === 'kategorie' && typeof subpage === 'number') {
				elements = _react2.default.createElement(_Category2.default, { data: _lodash2.default.find(_data.categories, { id: subpage }) });
			}

			return _react2.default.createElement(
				'main',
				{ className: _Main2.default.root, onClick: this.handleClick },
				elements
			);
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _this2 = this;

			_routeStream2.default.subscribe(function () {
				_this2.forceUpdate();
			});
		}
	}]);
	return Main;
}(_react2.default.Component);

exports.default = Main;