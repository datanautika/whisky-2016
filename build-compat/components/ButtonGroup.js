'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = ButtonGroup;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Button = require('./Button.css');

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ButtonGroup(props) {
	return _react2.default.createElement(
		'div',
		{ className: _Button2.default.buttonGroup },
		props.children
	);
}