'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _Category = require('./Category.css');

var _Category2 = _interopRequireDefault(_Category);

var _WhiskyName = require('./WhiskyName');

var _WhiskyName2 = _interopRequireDefault(_WhiskyName);

var _data = require('../data');

var _whiskyName = require('../utils/whiskyName');

var _whiskyName2 = _interopRequireDefault(_whiskyName);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Category = function (_React$Component) {
	(0, _inherits3.default)(Category, _React$Component);

	function Category() {
		(0, _classCallCheck3.default)(this, Category);
		return (0, _possibleConstructorReturn3.default)(this, (Category.__proto__ || (0, _getPrototypeOf2.default)(Category)).apply(this, arguments));
	}

	(0, _createClass3.default)(Category, [{
		key: 'render',
		value: function render() {
			var category = this.props.data;
			var categoryResults = _lodash2.default.find(_data.votesByCategory.values, { groups: { categoryName: category.name } });
			var selectedWhiskies = [];
			var whiskiesElements = void 0;

			if (categoryResults) {
				categoryResults.whiskyIds.forEach(function (whiskyId, index) {
					var whisky = _lodash2.default.find(selectedWhiskies, { whisky: { id: whiskyId } });

					if (whisky) {
						whisky.pointsCount += _lodash2.default.find(_data.votes, { id: categoryResults.voteIds[index] }).pointsCount;
					} else {
						selectedWhiskies.push({
							whisky: _lodash2.default.find(_data.whiskies, { id: whiskyId }),
							pointsCount: _lodash2.default.find(_data.votes, { id: categoryResults.voteIds[index] }).pointsCount
						});
					}
				});

				whiskiesElements = selectedWhiskies.sort(function (a, b) {
					if (category.id === 13) {
						// "Zklamání/propadák roku"
						var _result = a.pointsCount - b.pointsCount;

						if (_result !== 0) {
							return _result;
						}

						return (0, _whiskyName2.default)(a.whisky).join(' ').trim().localeCompare((0, _whiskyName2.default)(b.whisky).join(' ').trim());
					}

					var result = b.pointsCount - a.pointsCount;

					if (result !== 0) {
						return result;
					}

					return (0, _whiskyName2.default)(a.whisky).join(' ').trim().localeCompare((0, _whiskyName2.default)(b.whisky).join(' ').trim());
				}).map(function (selectedWhisky, index) {
					return _react2.default.createElement(
						'li',
						{ key: index, className: _Category2.default.whisky },
						_react2.default.createElement(_WhiskyName2.default, { data: selectedWhisky.whisky }),
						selectedWhisky.pointsCount ? _react2.default.createElement(
							'span',
							{ className: _Category2.default.pointsCount },
							selectedWhisky.pointsCount + ' ',
							_react2.default.createElement(
								'span',
								{ className: _Category2.default.pointsCountLabel },
								function (value) {
									if (value === 1) {
										return 'bod';
									}

									if (value === 2 || value === 3 || value === 4) {
										return 'body';
									}

									return 'bodů';
								}(selectedWhisky.pointsCount)
							)
						) : null
					);
				});
			}

			var selectedUsers = _data.users.map(function (user) {
				return {
					name: user.name,
					result: user.results.filter(function (result) {
						return result.categoryName === category.name;
					})[0]
				};
			}).sort(function (a, b) {
				return a.name.trim().localeCompare(b.name.trim());
			});

			return _react2.default.createElement(
				'div',
				{ className: _Category2.default.root },
				_react2.default.createElement(
					'h2',
					{ className: _Category2.default.heading },
					category.name
				),
				whiskiesElements && whiskiesElements.length ? _react2.default.createElement(
					'ol',
					{ className: _Category2.default.whiskies },
					whiskiesElements
				) : null,
				_react2.default.createElement(
					'section',
					null,
					_react2.default.createElement(
						'h3',
						{ className: _Category2.default.votesHeading },
						'Um\xEDst\u011Bn\xED'
					),
					selectedUsers.map(function (user, userIndex) {
						var elements = user.result.items.map(function (item, itemIndex) {
							if (item && typeof item === 'number') {
								return _react2.default.createElement(
									'li',
									{ key: itemIndex, className: _Category2.default.whisky },
									_react2.default.createElement(_WhiskyName2.default, { key: itemIndex, data: _lodash2.default.find(_data.whiskies, { id: item }) })
								);
							} else if (item && typeof item === 'object' && typeof item.id === 'number') {
								return _react2.default.createElement(
									'li',
									{ key: itemIndex, className: _Category2.default.whisky },
									_react2.default.createElement(_WhiskyName2.default, { key: itemIndex, data: _lodash2.default.find(_data.whiskies, { id: item.id }) })
								);
							} else if (item && typeof item === 'object' && (typeof item.name === 'string' || typeof item.description === 'string')) {
								return _react2.default.createElement(
									'li',
									{ key: itemIndex, className: _Category2.default.categoryItem },
									_react2.default.createElement(
										'p',
										{ className: _Category2.default.categoryItemName },
										item.name
									)
								);
							}

							return _react2.default.createElement(
								'li',
								{ key: itemIndex, className: _Category2.default.whisky + ' ' + _Category2.default.isEmpty },
								_react2.default.createElement(
									'p',
									null,
									'\u2013'
								)
							);
						});

						return elements.length ? _react2.default.createElement(
							'section',
							{ key: userIndex, className: _Category2.default.result },
							_react2.default.createElement(
								'header',
								{ className: _Category2.default.resultHeader },
								_react2.default.createElement(
									'h3',
									{ className: _Category2.default.resultHeading },
									_react2.default.createElement(
										'span',
										{ className: _Category2.default.resultHeadingLabel },
										'U\u017Eivatel'
									),
									_react2.default.createElement(
										'a',
										{ href: (0, _href2.default)('uzivatele', user.name) },
										user.name
									)
								)
							),
							_react2.default.createElement(
								'ol',
								null,
								elements
							)
						) : null;
					})
				)
			);
		}
	}]);
	return Category;
}(_react2.default.Component);

exports.default = Category;