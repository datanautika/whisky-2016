'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Button = require('./Button.css');

var _Button2 = _interopRequireDefault(_Button);

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MAILTO_REGEX = /^mailto:/;
var ROUTE_LINK_REGEX = /^\//;

var browserRouter = new _BrowserRouter2.default(_router2.default);

var Button = function (_React$Component) {
	(0, _inherits3.default)(Button, _React$Component);

	function Button() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, Button);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Button.__proto__ || (0, _getPrototypeOf2.default)(Button)).call.apply(_ref, [this].concat(args))), _this), _this.handleClick = function (event) {
			if (!(_this.props.link || MAILTO_REGEX.test(_this.props.link))) {
				event.preventDefault();

				if (_this.props.handleClick) {
					_this.props.handleClick();
				}
			} else if (ROUTE_LINK_REGEX.test(_this.props.link) && _this.props.useRouter !== false) {
				event.preventDefault();

				browserRouter.navigate(_this.props.link);
			}
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(Button, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(newProps) {
			return newProps.id !== this.props.id || newProps.name !== this.props.name || newProps.type !== this.props.type || newProps.link !== this.props.link || newProps.label !== this.props.label || newProps.badge !== this.props.badge || newProps.icon !== this.props.icon || newProps.selectedIcon !== this.props.selectedIcon || newProps.isLarge !== this.props.isLarge || newProps.isDisabled !== this.props.isDisabled || newProps.isSelected !== this.props.isSelected || newProps.isSubmit !== this.props.isSubmit || newProps.useRouter !== this.props.useRouter || newProps.handleClick !== this.props.handleClick;
		}
	}, {
		key: 'render',
		value: function render() {
			var buttonClass = _Button2.default.default;

			if (this.props.type === 'flat') {
				buttonClass = _Button2.default.flat;
			} else if (this.props.type === 'invisible') {
				buttonClass = _Button2.default.invisible;
			}

			if (this.props.size === 'small') {
				buttonClass += ' isSmall';
			} else if (this.props.size === 'large') {
				buttonClass += ' isLarge';
			}

			buttonClass += this.props.isSelected ? ' isSelected' : '';

			buttonClass += this.props.icon || this.props.selectedIcon ? ' hasIcon' : '';
			buttonClass += (this.props.icon || this.props.selectedIcon) && !this.props.label && !this.props.badge ? ' hasOnlyIcon' : '';

			var iconElements = [];

			if (this.props.icon) {
				iconElements.push(_react2.default.createElement(_Icon2.default, { key: 'icon', id: this.props.icon }));
			}

			if (this.props.selectedIcon) {
				iconElements.push(_react2.default.createElement(_Icon2.default, { key: 'selectedIcon', id: this.props.selectedIcon }));
			}

			var buttonElement = _react2.default.createElement(
				'button',
				{
					className: buttonClass + (this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
					onClick: this.handleClick },
				iconElements,
				this.props.label || null,
				this.props.badge ? _react2.default.createElement(
					'span',
					{ className: _Button2.default.badge },
					this.props.badge
				) : null
			);

			if (this.props.isSubmit) {
				buttonElement = _react2.default.createElement('input', {
					'class': buttonClass + (this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
					type: 'submit',
					name: this.props.name || this.props.id || '',
					id: this.props.id || this.props.name || '',
					value: this.props.label || '',
					onClick: this.handleClick
				});
			} else if (this.props.link) {
				buttonElement = _react2.default.createElement(
					'a',
					{
						className: buttonClass + (this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
						href: this.props.link ? this.props.link : '#',
						onClick: this.handleClick },
					iconElements,
					this.props.label || null,
					this.props.badge ? _react2.default.createElement(
						'span',
						{ className: _Button2.default.badge },
						this.props.badge
					) : null
				);
			}

			return buttonElement;
		}
	}]);
	return Button;
}(_react2.default.Component);

exports.default = Button;