'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _WhiskyDetail = require('./WhiskyDetail.css');

var _WhiskyDetail2 = _interopRequireDefault(_WhiskyDetail);

var _whiskyName3 = require('../utils/whiskyName');

var _whiskyName4 = _interopRequireDefault(_whiskyName3);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _data = require('../data');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', { minimumFractionDigits: 1, maximumFractionDigits: 1 });
var ML_IN_L = 1000;

var name = function name(whisky) {
	var _whiskyName = (0, _whiskyName4.default)(whisky),
	    _whiskyName2 = (0, _slicedToArray3.default)(_whiskyName, 3),
	    titlePart1 = _whiskyName2[0],
	    titlePart2 = _whiskyName2[1],
	    subtitle = _whiskyName2[2];

	return _react2.default.createElement(
		'h2',
		{ className: _WhiskyDetail2.default.heading },
		_react2.default.createElement(
			'span',
			{ className: _WhiskyDetail2.default.title },
			titlePart1 ? titlePart1 : null,
			titlePart2 ? _react2.default.createElement(
				'b',
				null,
				titlePart2
			) : null
		),
		_react2.default.createElement(
			'span',
			{ className: _WhiskyDetail2.default.subtitle },
			subtitle ? subtitle : null
		)
	);
};

var parseDate = function parseDate(dateString) {
	var partsCount = dateString.match(/-/g);

	if (partsCount <= 1) {
		return dateString;
	}

	var date = (0, _moment2.default)(dateString);

	if (partsCount > 1) {
		return date.format('Do MMMM YYYY');
	}

	return date.format('MMMM YYYY');
};

var WhiskyDetail = function (_React$Component) {
	(0, _inherits3.default)(WhiskyDetail, _React$Component);

	function WhiskyDetail() {
		(0, _classCallCheck3.default)(this, WhiskyDetail);
		return (0, _possibleConstructorReturn3.default)(this, (WhiskyDetail.__proto__ || (0, _getPrototypeOf2.default)(WhiskyDetail)).apply(this, arguments));
	}

	(0, _createClass3.default)(WhiskyDetail, [{
		key: 'render',
		value: function render() {
			var whisky = this.props.data;
			var regionClass = void 0;
			var regionName = void 0;
			var countryName = whisky.country;
			var isSingleMalt = whisky.type === 'Single Malt Scotch' || whisky.type === 'Single Malt Whisky' || whisky.type === 'Single Malt Whiskey';

			if (whisky.region) {
				regionClass = _WhiskyDetail2.default[whisky.region.toLowerCase()];

				if (whisky.region === 'Islay') {
					regionName = 'Islay';
				} else if (whisky.region === 'Islands') {
					regionName = 'Ostrovy';
				} else if (whisky.region === 'Highland') {
					regionName = 'Vysočina';
				} else if (whisky.region === 'Lowland') {
					regionName = 'Nížina';
				} else if (whisky.region === 'Speyside') {
					regionName = 'Speyside';
				} else if (whisky.region === 'Campbeltown') {
					regionName = 'Campbeltown';
				}
			}

			if (whisky.country === 'Scotland') {
				countryName = 'Skotsko';
			}

			var selectedUsers = _data.users.filter(function (user) {
				var use = false;

				user.results.forEach(function (result) {
					return result.items.forEach(function (item) {
						if (item && typeof item === 'number' && item === whisky.id) {
							use = true;
						} else if (item && typeof item === 'object' && typeof item.id === 'number' && item.id === whisky.id) {
							use = true;
						}
					});
				});

				return use;
			}).map(function (user) {
				var newUser = _lodash2.default.cloneDeep(user);

				newUser.results = user.results.filter(function (result) {
					var use = false;

					result.items.forEach(function (item) {
						if (item && typeof item === 'number' && item === whisky.id) {
							use = true;
						} else if (item && typeof item === 'object' && typeof item.id === 'number' && item.id === whisky.id) {
							use = true;
						}
					});

					return use;
				});

				return newUser;
			});

			return _react2.default.createElement(
				'div',
				{ className: _WhiskyDetail2.default.root + (regionClass ? ' ' + regionClass : '') + (isSingleMalt ? ' ' + _WhiskyDetail2.default.isSingleMalt : '') },
				name(whisky),
				_react2.default.createElement(
					'div',
					{ className: _WhiskyDetail2.default.leftColumn },
					_react2.default.createElement(
						'div',
						{ className: _WhiskyDetail2.default.row },
						whisky.country ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Zem\u011B'
							),
							_react2.default.createElement(
								'p',
								null,
								countryName
							)
						) : null,
						whisky.region ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Oblast'
							),
							_react2.default.createElement(
								'p',
								null,
								regionName
							)
						) : null
					),
					whisky.distillery || whisky.bottler ? _react2.default.createElement(
						'div',
						{ className: _WhiskyDetail2.default.row },
						whisky.distillery ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Pal\xEDrna'
							),
							_react2.default.createElement(
								'p',
								null,
								whisky.distillery
							)
						) : null,
						whisky.bottler ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'St\xE1\u010D\xEDrna'
							),
							_react2.default.createElement(
								'p',
								null,
								whisky.bottler
							)
						) : null
					) : null,
					whisky.age || whisky.vintage || whisky.bottled ? _react2.default.createElement(
						'div',
						{ className: _WhiskyDetail2.default.row },
						whisky.age ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'St\xE1\u0159\xED'
							),
							_react2.default.createElement(
								'p',
								null,
								whisky.age + ' let'
							)
						) : null,
						whisky.vintage ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Ro\u010Dn\xEDk'
							),
							_react2.default.createElement(
								'p',
								null,
								parseDate(whisky.vintage)
							)
						) : null,
						whisky.bottled ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Lahvov\xE1no'
							),
							_react2.default.createElement(
								'p',
								null,
								parseDate(whisky.bottled)
							)
						) : null
					) : null,
					_react2.default.createElement(
						'div',
						{ className: _WhiskyDetail2.default.row },
						whisky.strength ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'S\xEDla'
							),
							_react2.default.createElement(
								'p',
								null,
								NUMBER_FORMAT.format(whisky.strength) + ' %'
							)
						) : null,
						whisky.size ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Objem'
							),
							_react2.default.createElement(
								'p',
								null,
								whisky.size * ML_IN_L + ' ml'
							)
						) : null,
						whisky.caskNumber && whisky.caskType ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Sud'
							),
							_react2.default.createElement(
								'p',
								null,
								whisky.caskNumber + ' (' + whisky.caskType + ')'
							)
						) : null,
						!whisky.caskNumber && whisky.caskType ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Sud'
							),
							_react2.default.createElement(
								'p',
								null,
								'' + whisky.caskType
							)
						) : null
					),
					_react2.default.createElement(
						'div',
						{ className: _WhiskyDetail2.default.row },
						whisky.links && whisky.links.length ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.wideRowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Odkazy'
							),
							_react2.default.createElement(
								'p',
								null,
								whisky.links.map(function (link, index) {
									return _react2.default.createElement(
										'a',
										{ key: index, href: link },
										link
									);
								})
							)
						) : null
					)
				),
				whisky.image ? _react2.default.createElement(
					'figure',
					{ className: _WhiskyDetail2.default.rightColumn, style: {
							backgroundImage: 'url("' + (0, _href2.default)('assets', whisky.image) + '")'
						} },
					_react2.default.createElement('img', { src: (0, _href2.default)('assets', whisky.image), alt: '' })
				) : null,
				_react2.default.createElement(
					'section',
					{ className: _WhiskyDetail2.default.fullColumn },
					_react2.default.createElement(
						'h3',
						{ className: _WhiskyDetail2.default.votesHeading },
						'Um\xEDst\u011Bn\xED'
					),
					selectedUsers && selectedUsers.length ? selectedUsers.map(function (user, userIndex) {
						return _react2.default.createElement(
							'section',
							{ key: userIndex, className: _WhiskyDetail2.default.result },
							_react2.default.createElement(
								'header',
								{ className: _WhiskyDetail2.default.resultHeader },
								_react2.default.createElement(
									'h3',
									{ className: _WhiskyDetail2.default.resultHeading },
									_react2.default.createElement(
										'span',
										{ className: _WhiskyDetail2.default.resultHeadingLabel },
										'U\u017Eivatel'
									),
									_react2.default.createElement(
										'a',
										{ href: (0, _href2.default)('uzivatele', user.name) },
										user.name
									)
								)
							),
							_react2.default.createElement(
								'ol',
								null,
								user.results.map(function (result, resultIndex) {
									var category = _lodash2.default.find(_data.categories, { name: result.categoryName });
									var points = 0;

									result.items.forEach(function (item, itemIndex) {
										if (item && typeof item === 'number' && item === whisky.id) {
											points += category.points[itemIndex];
										} else if (item && typeof item === 'object' && typeof item.id === 'number' && item.id === whisky.id) {
											points += category.points[itemIndex];
										}
									});

									return _react2.default.createElement(
										'li',
										{ key: resultIndex, className: _WhiskyDetail2.default.whisky },
										_react2.default.createElement(
											'a',
											{ href: (0, _href2.default)('kategorie', category.id) },
											result.categoryName
										),
										points ? _react2.default.createElement(
											'span',
											{ className: _WhiskyDetail2.default.pointsCount },
											points + ' ',
											_react2.default.createElement(
												'span',
												{ className: _WhiskyDetail2.default.pointsCountLabel },
												function (value) {
													if (value === 1) {
														return 'bod';
													}

													if (value === 2 || value === 3 || value === 4) {
														return 'body';
													}

													return 'bodů';
												}(points)
											)
										) : null
									);
								})
							)
						);
					}) : null
				)
			);
		}
	}]);
	return WhiskyDetail;
}(_react2.default.Component);

exports.default = WhiskyDetail;