'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = {
	// platform
	CLIENT_PLATFORM: 'client',
	SERVER_PLATFORM: 'server'
};