'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _ModelsListHeading, _NotesListHeading, _NotesListNoteName, _NotesListAddButton, _NotesListDoneButton, _NotesListNoteCaptio, _ModelLoadedNotific, _CaseDataLoadedNot, _CaseDataLoadedNot2; /* eslint-disable quote-props */

var _constants = require('../../internals/constants');

var _constants2 = _interopRequireDefault(_constants);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EN = _constants2.default.EN;
var CZ = _constants2.default.CZ;

var i18nStrings = {
	// ModelsList
	'ModelsList Heading': (_ModelsListHeading = {}, (0, _defineProperty3.default)(_ModelsListHeading, EN, 'Models'), (0, _defineProperty3.default)(_ModelsListHeading, CZ, 'Modely'), _ModelsListHeading),

	// NotesList
	'NotesList Heading': (_NotesListHeading = {}, (0, _defineProperty3.default)(_NotesListHeading, EN, 'Notes'), (0, _defineProperty3.default)(_NotesListHeading, CZ, 'Poznámky'), _NotesListHeading),
	'NotesList Note Name {0}': (_NotesListNoteName = {}, (0, _defineProperty3.default)(_NotesListNoteName, EN, 'Note #{0}'), (0, _defineProperty3.default)(_NotesListNoteName, CZ, 'Poznámka #{0}'), _NotesListNoteName),
	'NotesList AddButton': (_NotesListAddButton = {}, (0, _defineProperty3.default)(_NotesListAddButton, EN, 'Add note'), (0, _defineProperty3.default)(_NotesListAddButton, CZ, 'Přidat poznámku'), _NotesListAddButton),
	'NotesList DoneButton': (_NotesListDoneButton = {}, (0, _defineProperty3.default)(_NotesListDoneButton, EN, 'Done'), (0, _defineProperty3.default)(_NotesListDoneButton, CZ, 'Hotovo'), _NotesListDoneButton),
	'NotesList NoteCaption': (_NotesListNoteCaptio = {}, (0, _defineProperty3.default)(_NotesListNoteCaptio, EN, 'Note:'), (0, _defineProperty3.default)(_NotesListNoteCaptio, CZ, 'Poznámka:'), _NotesListNoteCaptio),

	// notifications
	'Model loaded Notification Title': (_ModelLoadedNotific = {}, (0, _defineProperty3.default)(_ModelLoadedNotific, EN, 'Models loaded'), (0, _defineProperty3.default)(_ModelLoadedNotific, CZ, 'Modely načteny'), _ModelLoadedNotific),
	'Case data loaded Notification Title': (_CaseDataLoadedNot = {}, (0, _defineProperty3.default)(_CaseDataLoadedNot, EN, 'Case data loaded'), (0, _defineProperty3.default)(_CaseDataLoadedNot, CZ, 'Data o případu načtena'), _CaseDataLoadedNot),
	'Case data loaded Notification Text {0}': (_CaseDataLoadedNot2 = {}, (0, _defineProperty3.default)(_CaseDataLoadedNot2, EN, 'Case {0}'), (0, _defineProperty3.default)(_CaseDataLoadedNot2, CZ, 'Případ {0}'), _CaseDataLoadedNot2)
};

exports.default = i18nStrings;