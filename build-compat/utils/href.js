'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = href;
function href() {
	for (var _len = arguments.length, levels = Array(_len), _key = 0; _key < _len; _key++) {
		levels[_key] = arguments[_key];
	}

	return ('/whiskyRoku2016' ? '' + '/whiskyRoku2016' : '') + '/' + levels.join('/');
}